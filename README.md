# calender-slot-booking-service

Calendar Slot Booking service is similar to that of Calendly, which allows people
to define their available slots on a day and other people to book them.